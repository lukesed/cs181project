Prachie Banthia
Gabrielle Badie
Luke Sedney
Erick Deras
 
Requirements Elicitation Minutes
1.	Introductions
	a.	Project
		i.	Gabrielle: We have an idea for a project, and we want to hear from you. We believe there is 
			a problem when students need help, from either a professor or other students, about problems. 
			However, they may not be able to make it to mentor sessions or office hours, or they need help 
			at an inconvenient time.
	b.	Prof Yu: I am a professor, the new visiting assistant professor. I teach Algorithms and CS51. Helping 
			Rett with SML this year also.
	c.	Evan: Senior, math-CS double major. Mentor everything.
	d.	Diana: I go to Pitzer, my major is math and Spanish. 
	e.	Howard: I am a Pomona junior and a molecular biology major and organic chemistry mentor.
	f.	Gabrielle: You are all in technical fields, we notice after class sometimes there are questions when 
			there is a problem set . You have a class at the time of their office  hours. And you want to 
			get to know people in the class. You want space to teach and learn.  The internet is awesome. 
			We want it to connect people on campus. We would have a class specific website, which would 
			provide a QA solution to this website. LaTek compatibility, photos, diagrams, code, equations. 
			We want to know if you think this would be useful to you. Informal roles, former students, 
			website to help people. We want to hear from you.
2.	How do you do your classwork outside of class?
	a.	Howard: Check work after its been done, to help compare answers afterwards.
	b.	Diana: Depends on the material; problem sets at the caf�. I look for friends in the same classroom.
	c.	Evan: Depends on the atmosphere of the class. Some classes you help each other out---material or the 
			atmosphere? I learn best by struggling. 
	d.	Prof Yu: For algorithms I much rather the students should learn from each other; the programming 
			oriented classroom. I am used to working by myself because of grad school. It really really 
			depends on the course. You need to learn from you ...
3.	How are your office hours?
	a.	Prof Yu: The Sunday office hours are busy. It really depends on the convenience 
	b.	Evan: OH are only super useful; only if its convenience. 
	c.	Prof Yu: For my class, people make an effort to collaborate, sometimes you need to collaborate. 
	d.	Howard: If there's a mentor session, I will go. If I can't go to my mentor session, I have a partner 
			who does and that's the best we can do. I might make an appointment. It has been rescheduled. 
			It's a little more difficult this year.
4.	What do you do when you are stuck on something?
	a.	Evan: If I'm stuck, I sleep. 
	b.	Prof Yu: One thing I learned is that usually if you have to learn, you will learn if you keep trying 
			you will get it.
5.	Do you ever ask former students for help?
	a.	Howard: Usually not former students, it's easy to get stuff from other places; I need to prepare for 
			the mentor session. I don't ask for a problem set question, where the mentor session can help. 
			Students usually can semi-forget things after a while.
	b.	Evan: Allison Miller got me through Topology. I do use previous class takers, but only if convenient. 
	c.	Diana: Depends from the class, I ask for help. I understand the question, sometimes its stuck. 
6.	Would an online space be useful?
	a.	Evan: Only if its implemented well. I use Contact info/space text/email/phone, which is already more 
			stuff to check than I care for. I'd only use such a service if it's a lot of people because 
			otherwise I won't get it. It has to be so good that it's the only thing that's good, I got 
			four things to check. 1 thing to replace all the stuff. Or its invisible, or I can expect 
			results. 
	b.	Prof Yu: One of the latest innovations that's really trendy right now is to have a lot of discussion 
			form answer sites. HCI involved, adapts people's behavior, mobile-least amount of work. Try to 
			use that aspect, still make it as lazy as possible. 
7.	What do you think of a mobile app? 
	a.	Diana is nodding. 
	b.	Evan: Entering LaTex on the phone is awkward, but otherwise super useful. 
	c.	Diana: That's very convenient. 
8.	What programs do you use?
	a.	Howard: ChemDraw is a program used to create molecules to show and organize structures. A lot of people 
			collaborate on lab reports, you can't really say it out loud.
	b.	Evan: Pure vector images could be super useful. I don't know if anyone to solve thing, but use hand 
			drawings. 
	c.	Paint or scan a picture? Both ideas suck.
9.	Would you use stuff to interact with students?
	a.	Prof Yu: I actually don't mind. If I can use the mobile idea, the main problem gives me questions. I 
			would love to answer questions but I won't be answering because I' doing other things, and 
			then I feel bad. With a mobile app to answer questions-I would do it. And it only needs to 
			supports basic drawing, they aren't going to do details stuff anyway. 
	b.	Evan: Also, for CS, you should have syntax highlighting. 
10.	Have you guys used Piazza, Sakai, etc in terms of software you have used, how do you use it?
	a.	Evan: I was using ShareLatex when I was working with Prof Chen at first, but I quickly realized it was 
			easier to just go see her. Sakai, Piazza, --none worked because nobody used them. In many 
			environments, nobody will start because nobody is using it. 
	b.	Diana: It's easy to have face to face interactions.
11.	How often are you on the computer?  Are you in meetings?
	a.	Diana: I'm not at a computer, I don't come back til late, I use my phone but that's it. 
	b.	Howard: I have a computer in front of me always. The science building has great labs, and for many 
			upper division classes you need journal articles and so those labs are important.
	c.	Evan: I don't use them in meetings/class, but most of my homework and entertainment is on the computer. 
			It takes me 42 sec to check an email. 
	d.	Prof Yu: I am always online. I pretty much have to, because I don't want students to wait. Some 
			professors much rather have students talk to mentors, but I don't mind.
12.	Would this product be useful to you?
	a.	Howard: Organic Chemistry has 5 mentor sessions, I have friends ask me as a former student. Every 
			nonsocial night have a mentor session
		i.	Gabrielle: Does there really need to be a forum?
			1.	Howard: You have to do most of it on your own, its probably diff  and useful in GChem. 
					Ifyou don't get the concept its useful to discuss online
	b.	Evan: I'm taking a class at Scripps-if people would help me with calculus, thank would be nice. Its 
			late at night, nobody checks sakai, and the homework might be due tmrw
13.	Is email effective? What are thoughts on a chat feature within the class?
	a.	Diana: Email isn't effective, I'm a people person. Late at night, if I can see that others are up 
			would be useful.
	b.	Prof Yu: In terms of IM for classroom, as a professor, email me rather than chatting.
	c.	Diana: You have to think about it through email, sometimes it's hard to describe in actual word, you 
			want something more physically, and it takes time. You want something more visible. Draw app 
			or something, screensharing (Prof Yu: works for google interviews), I think a that is a good 
			idea
	d.	Howard: What incentive you give students to help out? I'm going to spend my time to do something else. 
			TAs are paid to mentor. Why would others get online? If TAs were required to be part of such 
			an online communities, what encouragement would you give? 
	e.	Evan: I don't think motivation is a problem. People feel  good that they helped out. Its like a tab of 
			facebook. If would be helpful to have former students. Its good to make it opt in, not opt out. 
			I would help. 
	f.	Howard: For the students that are not mentors, I just don't think they'd get much recognition, it's 
			something completely different every night, I have a feeling altuism wouldn't stay over time. 
			It's really the mentors that would get to know the students. 
14.	Is this a need?
	a.	Evan: It would be useful, but it doesn't stress me out.
	b.	Diana: I agree with Evan, it's unfortunate sometimes to miss office hours or mentor sessions so it 
			would be useful
	c.	Howard: It's not as useful at a small school, it would be a big problem at a big school. The TAs aren't 
			even that available. It's for everyone. 
	d.	Prof Yu: Well, for Pomona I feel bothered that I can't answer email fast enough. At a big school, they 
			don't feel bad if there time. At Pomona, I would feel bad if I don't answer questions here.
15.	Conclusion, any comments?
	a.	Nope
16.	Erick: Summary
	a.	Tell me if I say something wrong.
		i.	It depends a lot on what students classes. Some require individual time, and more math/cs 
			require a little more fast response-latex feature is useful.
		ii.	Features: ChemDraw, pure vector illus, centralized website, hand draw, social communication 
			between groups-screen sharing, 
		iii.	Diana: Screen sharing useful for TA, proof by induction would be nice to be able 
to see
		iv.	Howard: Khan academy software that they use for science classes. Students can draw it out 
			together, super useful. 
		v.	Fast response time, although website is a good idea, face to face is preferred, and mobile 
			is useful. 
17.	Kampe: Summary
	a.	Importance of critical mass
	b.	Convenience
	c.	Motivation of people with knowledge to participate
	d.	Response Time

Summary Report
1.	When and where the session took place and who attended.
	a.	When: 9/20/12 at 2:45-3:15pm
	b.	Where: Edmunds 101 at Pomona College
	c.	Who:
	i.	Panel
		1.	Diana Ortiz
		2.	Howard Lee
		3.	Evan Fields
		4.	Professor Louis Yu
	ii.	Team
		1.	Moderator: Gabrielle Badie
		2.	Scribe: Prachie Banthia
		3.	Facilitating: Erick Deras and Luke Sedney
2.	Info from open-ended information gathering.
	a.	Everyone seems to study and collaborate in different models. All four panelists had different strategies 
		for how much they worked together, how important mentor sessions were, and how much they worked alone. 
	b.	In general, people do not ask former students for help unless it's convenient
	c.	A critical mass is vital to get this project off the ground
	d.	Convenience is also critical, so a mobile app may be useful. However, it may have limited capabilities.
	e.	The students are always on their computers and require quick response times
	f.	Motivation is a concern: would people be willing to help?
3.	New requirement suggestions, in priority order
	a.	ChemDraw: Important to have something similar to draw diagrams. Will look into embedding capabilities.
	b.	Hand drawing: Very helpful but expensive. Will do research on potential third party options
	c.	Pure vector drawings: Only important to math, so therefore lower in priority. 
	d.	Screen sharing: Already done by many other vendors very well, so the priority is low.
	e.	Syntax highlighting for computer science: Only useful for computer science. Further, we do not want 
		people to upload their entire code to the site.
4.	Input on former requirements
	a.	LaTex is very useful
	b.	Everyone seems lukewarm about photo/video
	c.	Chatting is a higher priority than expected, people want to know if others are available


