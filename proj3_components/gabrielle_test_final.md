# Login / Settings Component Testing: FINAL

Primary Author: Gabrielle Badie

#### Assumptions:

The user model has been customized properly,
  and therefore we are only testing users' abilities
  to successfully log in and off, update passwords,
  and edit usernames.  We also assume that users can be correctly
  added and removed from the database.
  
I use the factory girl gem to create a dummy user.  This user
  has a dummy username and password which are valid
  and accepted by the system (i.e. email address contains
  text@example.com and password has minimum 6 characters).
  
  Load testing will be done during integration, as directed
  by the group, and will not be a part of this component's
  test strategy.
    
#### After Testing: 
  
  The factory girl gem creates a dummy user that is
  created and cleaned up by the system.  Cleanup
  is not required because all changes relate to
  the user specifically and will have no other
  impacts on the system.
  
#### Test Plan:

  For this component, I will be testing with rspec, supplemented
  by factory girl for rails, capybara and guard spec,
  well-established tools for Ruby on Rails.  Because my
  computer has Rails 1.8.x I will need to use an old version
  of factory girl, which only works with Rails v1.9.  This should
  all be specified in my Gemfile.
  
  In integration tests, ideally nothing is mocked or stubbed. Using
  the recommended setup for controller tests, for instance,
  is not a good idea in request specs. We want to click links
  exactly as a user would, fill in forms exactly how a user
  would, etc...
  
  To be clear, the views are tied specifically to the success
  of devise.  The controllers and attached responses have not
  been modified.  Therefore a change in the database is reflected
  by a success or failure message given by the system.  We rely
  on the success of devise for this component, and test aspects
  that were changed or modified for the purposes of what I have
  created and modified.  My tests are black box tests and test
  the database and the views.

## Test Cases:

### Login Page:

#### Test Successful Login Functionality

  Ensure that email and password fields are present,
  and when populated with correct credentials, should
  display successfull fign in message upon submit.
  
  Should also redirect user to welcome page.
  This is important because users need to log in.

#### Test Login Page Redirect

  This test works for users who are already logged
  in and should be redirected without having
  to reinput credentials.  Devise already has a cookie
  which determines if the user is working in a previous
  session that is logged in, and this information should
  be used correcty.

#### Error Handling for Login

  We should provide black box tests to ensure that
  specific error messages are displayed for the
  following mistakes and the page not refreshed
  
  * Email field is left blank
  * Password field is left blank
  * Email address is not in database
  * Email address is not a valid email address (doesn't contain right chars)

  We also want correct information to be displayed
  * There are email and password fields which,
    if given correct credentials, will log in successfully
  * Redirects User after user is signed in to welcome page
  * Contains forgotten password link
  
  Tests for this can be supported by rspec.
  
  We note that more tests were added to this page.  This
  is because I found some test-driven development helpful
  in writing the code for my component.  Also, success
  with devise and the controllers in the devise gem
  are tied directly to successful changes to users in the
  database.  Because the controllers for devise are
  hidden within the gem file and can not be modified, we
  can use the views to show changes in database values.
  This is consistent with all devise tests found online,
  because devise is a known application which will update
  user information effectively.

### Settings Page:

#### Test "Update Password" correctness

  We rely on the ability for users to correctly update their passwords
  therefore we test that updating passwords will correctly update the database
  and will be reflected at when users attempt
  to log into at the start of all following sessions.
  Message should be displayed that indicates user
  information was updated.

#### Test "Update Password" error-handling

  If password is not updated successfully, user must be notified
  and the previous password maintained or a temporary password
  generated.  This is handled by devise, so we will test
  that correct messages are displayed to reflect password
  changes.  We also ensure that correct messages are displayed
  for an invalid password (< six characters) and that
  user information is not changed.  Confirmation password
  must match given password.

#### Test "Update Username" functionality
  Users may want to change their usernames.  By default usernames
  are users' email addresses, which we check with a different test.
  We must determine that usernames are NOT email 
  addresses that don't match users' default email addresses.
  We use current dummy users to update their usernames
  and compare database states before and after updating
  to determine correctness.  No cleanup required.
	
#### Test "Update Username" error-handling

  If username is not updated successfully, user must be notified
  and the previous username maintained or a temporary username
  generated.  This is handled by devise, but to test this
  functionality we will also ensure that correct status messages
  are displayed.
  
More tests added during test-driven development to confirm that
the user must have visible fields to input information, and
that correct credentials will yield passing results.

### Addendum Notes

12/16/12

Revisions and additions can be tracked through github, but I
wanted to confirm that tests were not removed from my original
plan, which was approved by the group and grader.  In fact,
tests were only added to this plan to cover more cases.
There are 14 tests total in my test suite.  Test
results given in '/test_results.txt'.