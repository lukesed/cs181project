### The Feed Component Review Report
##### Author: Gabrielle Badie

This meeting was held on Friday, November 30 and attended by
Gabrielle Badie, Erick Deras, Prachie Banthia and Luke Sedney.
The meeting was run by Luke Sedney and Gabrielle Badie acted as scribe,
in reviewing the feed specs, design and tests.

##### Summary of points:

Issue: There is no indication that events will be filtered or separated
by date.  We suggest that there be filtering of posts by 'this week'
and 'older'.  Must fix.

Defect: There needs to be a distinction between announcements
and questions visually in the feed, indicating the difference
between instructor and student posts.  Updating this component
relies on similar updates to Erick's component.  This distinction should be reflected in the 
design and tests.  Must fix.  Note: There will still just be a 'new post' button to create
a new post.

Defect: Files should be in an apps/views/posts folder, not apps/views folder.  This
is a must-fix item to integrate with other components.

Issue/Specification: Aesthetics need to be accounted for with the appropriate
html and css.  This is a must-fix item.

Defect: There should be no edit and delete buttons on the posts.  These are not specified 
elsewhere in this project.  Should-fix item, and tests should be removed in addition to their
inclusion in the design.

Suggestion: Timestamps should be discussed in design and specs.  Should fix.

Question: what is being displayed?  Just question titles or text of the content?
This should be clarified.  Should fix.

Question:  Will there be a search bar?  Is this a part of this component? Prachie needs
to clarify that she is NOT doing the search bar in specs, design and tests.  Should fix.

Suggestion: Update reasons for no white box testing.  There are no side effects
to your code therefore black box tests are sufficient because they determine if the pre- and post-conditions are satisfied.  Comment.
