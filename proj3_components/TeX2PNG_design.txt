/*
Tex2png Component Design
Luke Sedney

TeX2PNG is a Unix utility that takes TeX code and returns PNG images of the
expressions that the code specifies. We will require it so we can allow users to
embed LaTeX in their posts. The component consists of Ruby code that scripts the
operation of tex2png. When it receives a post with LaTeX, the Rails controller
responsible for adding messages will call this code to render the images, then
embed the images in the post at the point where the code was entered.

Note that this document does not actually use valid Ruby function declarations.
The Java/C style is used to show the return types and access specifiers in a 
single line. */

The Kernel module will be used to run Tex2png, and the File and URI modules to
manage the image and TeX files.

require "aws-s3"
/* This package is necessary to use S3. */

require "image_size"
/* Allows image dimensions to be checked */

require "timeout"
/* used to place a limit on waiting for system call */

require "open3"
/* needed to get system io */

require "thread"
/* needed for queue */

texFailedQueue 
/* A queue of elements to be processed in the event that TeX2PNG fails. Each
element of this queue will include the code to be processed, and the key on S3
where the dummy image has been stored. */

public string new_image( string code, (optional) string key)
/* This function takes tex code as a parameter and an optional key string.  If
there is no key, it generates one. It calls the tex2png utility to convert the
file, with parameters instructing it to put the output in "./output" in the
correct format. The image is checked to be within the dimensions 640x480, and if
it is not, a locally stored error image stating this is used. If tex2png returns
a syntax error, this program uses a locally stored image stating that the TeX
syntax is incorrect, and that only the amsmath package is available (in case the
user input TeX that was valid for other packages). If the call to tex2png
otherwise fails, an error image is used as the output, the input code and the S3
key are added to texFailedQueue.de This function calls imageToS3 with
the location of the image file and the key, and the input code file is deleted.
*/

private void image_to_s3( file image, string key)
/* The image in the file is uploaded to S3, and the local image file is deleted. */

public void process_tex_queue()
/* This function should be run by a system administration when tex2png
functionality is restored after a failure. It calls new_image on each element
with the image and key stored for each item in the queue. */


