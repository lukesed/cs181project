Feed Component Test Plan

Overview:
This component should display the feed entries of the datatable in chronological order from most recent to least 
recent post. Each post should display its subject, but not its content. Each post should also have an type and time.
The component should also have a clickable Create Post button that does not have any further functionality in this 
component.

Index:

Framework: As in other languages, Ruby provides a framework in its standard library for setting up, organizing, 
and running tests called Test::Unit. Test::Unit provides a way to define tests and run them as a group. We will 
also utilize rspec and cucumber, which as a popular and well-established testing tool for Ruby web applications.

Test Cases:

NOTE: CHECK GITHUB COMMENTS FOR REASONING FOR CHANGES.

Black box: Our output checked in the output Feed and the datatable in the database.
1. Check routing for :index and :seemore pages
Justification: routing to the correctweb pages is critical to use
Set-up: none
Test: Check if we can get to the pages successfully
Correctness: Successful route to the page

2. Test that correct number of posts are displayed
Justification: This situation covers code in the viewer index.html.erb, which is critical, as well as the def show and
index from the posts_controller.rb
Set-up: There should be a given number of entries in test database, and we check if they are displayed
Test: Compare the Feed entries displayed to the timestamps in the datatable.
Correctness: w2 entries should be noted on page

3. Test that the type of the posts are correctly being displayed
This checks the code in the viewer index.html.erb as well as the def show and index from the posts_controller.rb
No special set up or clean up required
We just need to have entries in the datatable
Test: Compare Feed to datatable's post types and ensure there exactly that post type is displayed.
Correctness: The correct type is displayed for each entry displayed.

4. Title is correctly displayed
Justification: This is needed to test some html/css formatting and consistency
No set up
Test: Check the title of the pages
Correctness: Title should be exactly 'QED'


White box:
I argue that my component has no white box testing requirements. In white box testing, I know the code?s details. 
However, I notice in trying to create the white box tests that Black box testing of the outward functionality in 
regards to the Feed webpage and the datatable is enough to do complete code coverage on the component. The main 
blocks of code in my component are as follows:

* posts_controller.rb: the def (aka methods) index, show and destroy

* show.html.erb: display of subject, content, and timestamp (this component does not require full testing because it 
is a dummy component to ease integration)

* index.html.erb: display each post in reverse chronological order, have links displayed for Create Post, Edit Post, and
Delete Post

I argue that posts_controller.rb's index and show methods are fully tested by test cases 2 and 3. The destroy 
method is tested by test case 5 and 6. show.html.erb is simply a dummy to ease integration with another component.
index.html.erb is tested by test cases 1-4 in all branches and cases. The initialization of the output webpage is 
tested by case 1. Thus, we do not need white box testing in this case to test all the code. However, we can easily 
use the black box tests as white box tests as well, ensuring that the correct lines of code are being tested with the
correct test to ensure that our hypothesis that we are covering all the code with those tests is correct. Further, There are 
no side effects to the code therefore black box tests are sufficient because they determine if the pre- and post-conditions 
are satisfied.
has any side effects 
