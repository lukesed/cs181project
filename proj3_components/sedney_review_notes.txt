Luke Sedney
QED design review notes


Login Page and Settings

Design:

This is really detailed, but some parts read like installation instructions,
which I don't think are in scope. Maybe I'm misinterpreting and/or that will be
turned into code at some point.

line 108: I thought banned users were just deleted? 

Tests:

There is good testing of the logic and operations performed, but what about
testing for the existence and characteristics of UI elements? I think this is a
major part of Rails testing, and is offered by utilities like minitest:spec and
most notably rspec.

What testing tools are you using?


Post Management

Specification:

Is this from before you and Prachie revised what components you are working on?

Design:

line 106: what is "in memory"?

Tests:

You talk about using RSpec, but I have the same comment as about Gabrielle. 
There is good functionality testing but no mention of the UI.


Feed

Specification:

This seems to describe a combination of feed and viewer specifications.

Design:

Side note: I think there is some kind of text encoding issue here.

line 12: Why does every post have an edit and delete button, and why are they 
in the feed?

Tests:

If the edit functionality is part of this component, it should be tested.

I don't think code coverage proves that the black box cases are complete.
Covering a line of code in one case doesn't show that it is correct. If I'm
understanding the design correctly, I think you could more easily approach this
proof by talking about the lack of side effects and data internal to the
methods.
