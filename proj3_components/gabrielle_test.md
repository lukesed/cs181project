# Login / Settings Component Testing

Primary Author: Gabrielle Badie

#### Assumptions:

The user model has been customized properly,
  and therefore we are only testing users' abilities
  to successfully log in and off, update passwords,
  and edit usernames.  We also assume that users can be correctly
  added and removed from the database.
  
#### Before Testing: 

  Before running tests, we will have created
  a set of dummy users assigned to dummy classes which
  we have also created.  This means that these tests only deal
  with management of existing user accounts.
  
#### After Testing: 
  
  Because all changes should be contained within
  the users model, we will only need to remove the dummy
  users that were created before running these tests.
  We must also remove the dummy classes created by these
  tests.

#### Test Plan:

  For this component, I will be testing with cucumber,
  a well-established tool for Ruby.  There are also tests specific
  to devise that have been established with cucumber, which
  I will be employing.

### Test Cases:

#### Default Student User correctness
  
  * Must be added to appropriate class (class must
  be designated for students to be added).
  * Must have default username as email address
  * Must have have unique email address
  * Must have unique username
  
  These attributes are handled by devise, which
  we assume to work correctly, although because
  they encompass customizations to devise's default
  user model we must add users to the database, which
  we will do under dummy classes, and view database
  entries before and after adding users
  to determine that the previous attributes are, in fact, present
  and correct.  
  
  
#### Bach Invite Input correctness

  Input is verified as a list of correct email addresses
  and split correctly into email addresses.  This will be used
  to generate emails (users are invitable) so it is important
  that we verify a text input can be checked as a set
  of email addresses.
  
#### Batch Invite User-Type Correctness
  
  * Verify that students are added with the correct user
  role, i.e. 'student' or 'student_assistant'.  We can
  do this by inviting new and existing users to classes
  and viewing database entries before and after this process.
  Batch invite input correctness is required before testing
  this component effectively.  No cleanup is required because
  this may be used for further tests.

#### Test "Update Password" correctness

  We rely on the ability for users to correctly update their passwords
  therefore we test that updating passwords will correctly update the database
  and will be reflected at when users attempt
  to log into at the start of all following sessions.
  We will compare the Users table before and after
  updating the password to make sure that changes are correctly
  reflected.  No cleanup required as 
  None required. Inserted post may help us for the next test.


#### Test "Update Password" error-handling

  If password is not updated successfully, user must be notified
  and the previous password maintained or a temporary password
  generated.  This is handled by devise, but to test this
  functionality we will store and update user password user
  information in the given user tables.

#### Test "Update Username" functionality
  Users may want to change their usernames.  By default usernames
  are users' email addresses, which we check with a different test.
  We must determine that usernames are unique and are NOT email 
  addresses that don't match users' default email addresses.
  We use current dummy users to update their usernames
  and compare database states before and after updating
  to determine correctness.  No cleanup required.
	
#### Test "Update Username" error-handling

  If username is not updated successfully, user must be notified
  and the previous username maintained or a temporary username
  generated.  This is handled by devise, but to test this
  functionality we will store and update user username user
  information in the given user tables and view this information
  before and after tests.  No cleanup required because dummy
  users may be used for other tests of this component.

#### Test "Remove User from Roster" functionality
  Instructors may want to remove users from their
  classes.  This would require classes to no longer
  maintain that user's id in its roster list
  and for users' class list to no longer reflect
  enrollment in this class.
  We use current dummy users
  and compare database states before and after updating
  to determine correctness.  No cleanup required
  because these dummy users and classes may be used
  for other tests.
  We must also determine that no users are deleted
  in the process, only that user tables reflect
  updating of class enrollment
  and that class tables reflect updated user enrollment.