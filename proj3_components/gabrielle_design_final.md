# Login / Settings Component Design: FINAL

Primary Author: Gabrielle Badie

## Component Overview

This component manages the users' ability to login as well as create and manage
individual user profile information.  It works with two view pages:
the initial splash page which allows all users to log in to their accounts,
and the user setting page which has two forms: one allowing users to edit
their usernames, and another to allow users to update their passwords.
These components will gather input from the user and effectively
check/update user table information.

Files: Ruby, by default, organizes files into the following folders -- 
app, config, doc, lib, log, public, script, test, tmp, 
vendor and db.  The following component pieces will be contained
in the app, db and config folders.

### User Table

We will use Devise will generate the following User models and files 
by default with the bash command

```
rails generate devise User
```
This will execute the following:
* invoke active_record
* create app/models/user.rb
* invoke test_unit
* create test/unit/user_test.rb
* create test/fixtures/users.yml
* inject app/models/user.rb
* create db/migrate/2010041206912_devise_create_users.rb
* route devise_for :users

We will be updating app/models/user.rb to add columns for 
usernames [and classes, although that is out of the
scope of this component], and db/migrate/devise_create_users.rb
to ensure that users are encryptable, lockable, and invitable.

First we provide the following list of the
columns automatically generated by Devise, updated to include
a column for usernames and can be used to log in.
After updating these files with the appropriate user model, we will have
to rake db:migrate our user database to update our user model.

```ruby
create_table(TABLE_NAME) do |t|
  ## Database authenticatable
  t.string :email,              :null => false, :default => ""
  t.string :encrypted_password, :null => false, :default => ""

  ## Recoverable
  t.string   :reset_password_token
  t.datetime :reset_password_sent_at

  ## Rememberable
  t.datetime :remember_created_at

  ## Trackable
  t.integer  :sign_in_count, :default => 0
  t.datetime :current_sign_in_at
  t.datetime :last_sign_in_at
  t.string   :current_sign_in_ip
  t.string   :last_sign_in_ip

  ## Encryptable
  # t.string :password_salt

  ## Confirmable
  # t.string   :confirmation_token
  # t.datetime :confirmed_at
  # t.datetime :confirmation_sent_at
  # t.string   :unconfirmed_email # Only if using reconfirmable

  ## Lockable
  # t.integer  :failed_attempts, :default => 0 # Only if lock strategy is :failed_attempts
  # t.string   :unlock_token # Only if unlock strategy is :email or :both
  # t.datetime :locked_at

  # Token authenticatable
  # t.string :authentication_token

  ## Invitable
  # t.string :invitation_token

  t.timestamps

end
```
Each user will have a role
which we set up in our Models
```ruby
class User < ActiveRecord::Base
  belongs_to :role
end
class Role < ActiveRecord::Base
  has_many :users
end
```
and in db/seeds.rb:
```ruby
['student', 'instructor'].each do |role|
  Role.find_or_create_by_name role
end
```
(then $ rake db:seed)

We set the default role, student, as a model callback
```ruby
class User < ActiveRecord::Base
    # sets the default role
end
```

Furthermore, in db/migrate we will require

*class DeviseCreateUsers < ActiveRecord::Migration*
to update the user table with columns for usernames and class IDs (in
file devise_create_users.rb) as well as


*class DeviseInvitableAddToUsers < ActiveRecord::Migration* in
devise_invitable_add_to_users.rb
manage class invites and batch invites to classes.
* This is not part of this component. *

This gives us the customized user profile that we want and will need
to use for this component.

## Security

Rails uses a cookie for its sessions, so we ensure that the entire
website uses SSL for security reasons.  Rails 3.1 does not need the
ssl_requirement gem and instead we include

```ruby
#in config/environments/production.rb
config.to_prepare { Devise::SessionsController.force_ssl }
config.to_prepare { Devise::RegistrationsController.force_ssl }
config.to_prepare { Devise::PasswordsController.force_ssl }
```
and enable SSL on the Apache server.  This would be done for
a complete project, however because this website will be locally
hosted for the purposes of this component, this is only
included for real-world insight.

## Customization

### Email addresses
We want email addresses to be case insensitive, so we add
the following line of code to config/initializers/devise.rb
```ruby
# config/initializers/devise.rb

...
config.case_insensitive_keys = [:email]
...
```

### Logging In / Splash Page

This page is automatically generated by Devise, stored in 
the app/views folder.  This only needs to be updated for cosmetics,
including the QED logo, although this page will not
include the header shown on the other pages of the website,
and needs email and password fields with 'submit' and 'forgot password'
buttons.  Forgot password redirects to a different form, which
is also described below.

We will need to update devise/sessions/new.html.erb to implement
the following customization.  If a user is already logged in,
going to the front page will redirect to the list of their classes,
regardless of whether the student is in the instructor or student role.
This code is straightforward and does not require pseudocode -- we only
require that it be included in this html file.

If a user forgot their password, we will also need to redirect
them to a page which has a password form.

#### View for Login Page:

Every page will check to see if user is already logged in and
redirect them appropriately

```html
<!-- in app/views/layouts/application.html.erb -->

<!-- check if user is signed in or not and redirect appropriately -->
```

```html
<!-- in app/views/devise/sessions/new.html.erb -->

<%= form_for(@user, :url => { :action => "login" } ) do |f| %>
    <!-- field for email address -->
    <!-- field for password -->
    <!-- action_container submission button -->
    <!-- forgot_password button redirects to forgot_password page -->
<% end %>
<%= render "devise/shared/links" %>
```

Where "devise/shared/links" includes the link for forgotten passwords

```html
<!-- in app/views/devise/shared/_links.erb -->

<!-- given passwords recoverable and no input, show active forgot password link -->
```

which will take us to the following page

```html
<!-- in app/views/devise/passwords/new.html.erb -->

<%= form_for(@user, :url => { :action => "forgot_password" } ) do |f| %>
    <!-- field for email address -->
    <!-- dummy submission button while this is locally hosted -->
<% end %>
```

### Updating Passwords and Usernames

Note that controller actions are already within
the devise gem file and will not need to be customized or
inherited from in this project.  The views, however
can be modified, and generating the correct user tables
for display names, etc. is an essential part of this
component.

#### View for Editing User Info:

```html
<!-- in app/views/devise/registrations/edit.html.erb -->

<%= form_for(resource, :as => resource_name, :url => registration_path(resource_name), :html => { :method => :put }) do |f| %>
    <!-- field for email address -->
    <!-- field for new username -->
    <!-- field for new password -->
    <!-- field for new password confirmation -->
    <!-- field for current password -->
    <!-- action_container submission button -->
<% end %>
```
