DESIGN

Note: Erick Deras and Prachie Banthia worked very closely on their components to ensure functionality, so the 
component pages are often shared.

Assumptions: The toolbar on the top of the webpage has been created and a specific class has been chosen by the
user from those available. Suppose the datatable of this specific class has already been populated with 
information (i.e. questions). We assume the chosen class is valid for the student.

Description: This component creates the Feed from the table, displaying the feed entries in chronological order
from most recent to least recent post. Each post will display its subject, but not its content. Each post will 
also have an "Edit" clickable button, whose back-end functionalities are not within the scope of 
this component but whose purpose is obvious. For the Feed, there will also be a clickable "Create Post" button and a 
"Search" box, whose functionalities are also not within the scope of this component but whose purpose is obvious. 
To ensure compatibility with the Viewer component, this component should also link from each entry in the 
Feed to the more detailed information in that entry of the table. 

FILES: Ruby creates projects such that projects by default have this organization: app, config, doc, lib, log, 
public, script, test, tmp, vendor, db. We focus on the app (i.e. applications) folders in this component.
db folder:
We use a "post" structure in the table. We use ruby's migration tool to create the table for testing purposes. 

We should explain the db (ie database) folder's organization for ease in understanding the rest of the design:

schema.rb:
ActiveRecord::Schema.define(:version => ###) do
  create_table "posts":
    t.string   "title"
    t.text     "content"
    t.text     "comments"
    t.string   "user"
    t.string   "displayname"
    t.float    "attachment"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end
//these are the schema characteristics we keep per class
end

seeds.rb-contain default value for database (should be empty)

use migrate tool to create-
###_create_posts.rb-
class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.text :comments //will be serialized
      t.string :user
      t.string :displayname
      t.float :attachment
      t.timestamps //this is the schema type choices
end everything


Now we can move on the the app folder.
app folder: includes assets, controllers, helpers, mailers, views, and models. We focus on the controllers and
views folders within the app folder.

controllers folder: 
application_controller.rb- 
class ApplicationController < ActionController::Base
// include database security protection (ie. call protect_from_forgery)
end

posts_controller.rb-
class PostsController < ApplicationController
  def index
		//show all posts
	end	

	def show
		//show a specific post
	end

	def new
		//not using in this component
	end

	def create
		//not using in this component
	end

	def edit
		//not using in this component
	end

	def update
		//not using in this component
	end

	def destroy
		// not used
	end

	def seaall
		// displays all the posts in the database
	end

end

**Note: The methods not defined in this component are defined in the Post Management Component being written
by Erick. This class will be an integration effort between the two components.
------------------------------------------
views/posts folder:
show.html.erb: only implemented for testing purposes. Will display a psuedo-Viewer: display a specific post�s 
detail when it is clicked on in the Feed.
Show post.title
Show post.content
Show post time created

edit.html.erb- not in scope of this component
new.html.erb- not in scope of this component

index.html.erb-
psuedocode:
for each post do:
	if (created_at < 1 week ago)
		display post name
		link_to from post.title to the full post details (this link is  dummy for now, to be integrated with another component)
		display post.title
link_to new_post_path for "New Post" link within a new post Button
link_to see_more_path for "See More" link within a see more Button

seall.html.erb-
psuedocode:
for each post do:
	display post name
	link_to from post.title to the full post details (this link is  dummy for now, to be integrated with another component)
	display post.title
link_to new_post_path for "New Post" link within a new post Button





