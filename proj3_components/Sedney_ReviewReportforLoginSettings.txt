This meeting was held on Thursday, November 29th at 7pm. 

Attending was Gabrielle Badie, the author of the component, Luke Sedney, who
acted as scribe, Erick Deras, the moderator, and Prachie Banthia.

The subject of this review was the specifications, design, and test plan for the
login page and settings.

Issues:

Gabrielle decided to remove the part of the component dealing with a class
roster to reduce the complexity.

We determined that the default display name should be the email address, to make
signing up as simple as possible. There will be an option to change the display
name in the settings. There will be a requirement that all display names be
distinct, as users would not be able to distinguish between others of the same
name.

There should be more test cases, including tests of existence and
characteristics of UI elements. Test::unit will be utilized for this purpose.

Questions: 

There was some confusion about whether students removed from classes by
instructors would be in some way marked as banned, or deleted from the class in
the database. They will be removed from the class.

We need to determine whether each of us should perform load testing
individually, or if it will take place during integration.

The component is approved with required changes. In particular, more tests are
required, and a few clarifications need to be made in the design about the
changes noted above.

**Note: during implementation, it became clear that requiring display names to
be unique would significantly increase the complexity of the component, so the
group agreed to remove this requirement.
