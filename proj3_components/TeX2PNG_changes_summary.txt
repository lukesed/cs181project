
Summary of changes in response to design/specs/test plan review.

Defect 1 and Issue 1: I have noted the use of the amsmath LaTeX package. We
can't determine whether the TeX is malformed or using another package, so both
cases must result in the same error. The particular syntax error will not be
given - just an error image saying the code could not be parsed, and only
amsmath is available. This functionality is tested.


Defect 2: We specify that image dimensions must be within 640x480, and error
images will be returned if an image is too wide and/or high. This functionality
is tested.

Issue 2: I have added references to ruby gems and modules that will be used.

Suggestion: I updated the test plan to specify the use of Test::Unit, so we all
will be using the same test framework.

Question: I have decided not to include load testing, as this would seem to test
performance characteristics of the machine used in development that wouldn't be
comparable to an EC2 instance. Doing load testing in an actual EC2 instance
could incur significant cost, as users are billed for IO operations and
bandwidth. Also, setting up the instance may be outside the scope of this
project.
