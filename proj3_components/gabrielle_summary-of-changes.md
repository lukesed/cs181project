# Summary of Changes made to Login / Settings Component Specs/Design/Test

Primary Author: Gabrielle Badie

Removed component for inviting students because this was not
fully determined before specs/design/tests were written and we 
decided to reduce the complexity of this component by doing so.
We note that all information about adding and deleting users was removed.

Updated information about the Forgotten Password functionality and page
and added black box tests to ensure that this was correct.

Clarified all tests to make them more complete and include both black
and white box testing.  Added tests and tests for UI components including
use of Test::unit.

Ensured that display names will be distinct in the design of this component.
Added modification of display names to this component.

Removed 'banned' as a user type from user tables.

Added a sentence about load testing, reflecting this group's decision
to do it during integration and not in individual components.