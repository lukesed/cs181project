# Review Notes
## Author: Gabrielle Badie

### Post-Management
##### Erick Deras

I greatly appreciated the division of work into the file system we assume will be present.  Questions I had:
I think the distinction between posts needs to be made more clear.  We have two types of posts:
questions posted to the class, and comments on those questions.  It was unclear to me which you were
focusing on more (although clearly, I think, you did talk about posting a new question to the class)
but a distinction needs to be made.

Another distinction I am worried about.  When a post is made, at lest in the UI made for the architecture
users were distinguished by students, instructors and student_assistants.  Will you reflect the user's
role when they post?  This should be added to your table and this information should be retreived.
TODO: We need to have a larger group discussion about the architecture of distinguishing
students and student_instructors
relating to their enrollment in classes.

Also in the views folder, 'new.html.erb' is the title of a page not usually given to a new post.  Usually
'new.html.erb' is a file under views/users/sessions or something like that to determine a new user session.
Maybe put this in its own folder in views, for organization, or rename it.  Actually, all of these files
might want to go in their own folder in views to organize our files.

For Erick and Prachie: How will time stamps work?  With time zones, where will it be localized?  Will we merely be showing a time or will we say '2 minutes ago' etc.?  I think Ruby can deal with this,
although it should be made clear.

Why are you testing Luke's component explicitly?  I know that it is
integrated with yours, although you should
test more that integration not the component.  Lastly, how will this all fit into page design/UI?  This is a page component, not a page.  How will this be written?

### Feed on QA Page
##### Prachia Banthia

Prachie, you wrote in your specifications that

> Also, once the class or study group is chosen, a user can use a search bar to search posts for
> specific strings, and these search results will be displayed in the Feed. This component’s role
> in the system is critical. This component allows the user to view different information that they
> are privy to. This component is centered, therefore, on the interaction between the user, the
> web interface, and the Apache server. 

however search functionality is not discussed in your design.  The design should be updated to reflect
these changes, and tests made.  Also--announcements?

To clarify...the two methods you are doing are 'index' and 'show' in posts_controller.rb?  In addition 
to work in show.html.erb and index.html.erb?  You appeared to summarize in your test file
although I would have liked this to be clearly stated in the design from a readability standpoint.
Will users have to refresh the page for new posts to be shown?  i.e. posts that have been written
and published by other users since they first loaded the page?

Many of the more general comments on Erick's apply to yours also Prachie.  Thank you also for organization.

### TeX to PNG
##### Luke Sedney

I do appreciate the appeal to writing methods with comments.  Makes it clear
exactly what you need to fill in.  Would like to see more tests as to what invalid TeX code is.  Code must be processed quickly--
can you test for speed and/or a maximum threshold?

How will this fit into the file system and integrate with other components?

Design looks good, but I would have preferred more detail, and I am not sure tests are complete.
