require 'test_helper'

class PostsControllerTest < ActionController::TestCase

test "should get index" do
  get :index
  assert_response :success
  assert_not_nil assigns(:posts)
end



test "should get see more page" do
  get :seeall
  assert_response :success
  assert_not_nil assigns(:posts)
end


test "title is QED" do
get :index
assert_select "title", {:count => 1, :text => "QED"},

end

test "two posts are displayed" do
get :index
assert_select 'a', "Announcementdude"
assert_select 'a', "TestPost"

end

test "announcement displayed" do
get :index
assert_select "h2", "Announcement: Announcementdude"

end

test "should get new" do
	get :index
  post :new
  assert_response :success
  assert_not_nil assigns(:posts)
end

test "should create post" do
	get :index
  post :new
  before = Post.count
  post :create, :post => { :title => 'Hi loser dude dude', :content => 'hwocuhce'}
	after = Post.count
	assert_equal after, before + 1
end

test "title empty" do
	get :index
  post :new
  before = Post.count
  post :create, :post => { :title => '', :content => 'hwocuhce'}
	after = Post.count
	assert_equal after, before
end

test "title 1 character" do
	get :index
  post :new
  before = Post.count
  post :create, :post => { :title => 's', :content => 'hwocuhce'}
	after = Post.count
	assert_equal after, before
end

test "content empty" do
	get :index
  post :new
  before = Post.count
  post :create, :post => { :title => 'hello', :content => ''}
	after = Post.count
	assert_equal after, before
end

test "title taken" do
	get :index
  post :new
  before = Post.count
  post :create, :post => { :title => 'TestPost', :content => 'sup yo'}
	after = Post.count
	assert_equal after, before
end


test "latex is recognized" do
	get :show, :id => 3
	assert_select "img"
end

end




