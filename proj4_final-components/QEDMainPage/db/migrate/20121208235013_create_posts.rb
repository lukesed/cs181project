class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.text :comments
      t.string :user
      t.string :displayname
      t.float :attachment
      t.integer :role

      t.timestamps
    end
  end
end
