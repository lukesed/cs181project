class Post < ActiveRecord::Base
  attr_accessible :attachment, :comments, :content, :displayname, :role, :title, :user

  validates :title, :content, :presence => true
  validates :title, :length => { :minimum => 2 }
  validates :title, :uniqueness => true
end
