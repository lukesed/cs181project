To View Webpage:
download Ruby on Rails v 1.9.2
run runme.bat
view at http://localhost:3000/posts/ (or whatever port you get to). This may take a while, so wait 2-3 minute after running the .bat file, which will start the server, so view http://localhost:3000/posts/.

To Run Test Suite:
Run RUN_TEST_SUITE.bat
Should show no errors, no failures, and a bunch of assertions.


