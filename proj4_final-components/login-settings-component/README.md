Login/Settings Component
========================

After using the 'rails s' command, login and settings
pages can be viewed in a web browser at the default location
http://localhost3000/users/sign_in and
http://localhost3000/users/edit.  These are the
two pages required for this component.

The database can be viewed and modified directly with the
'rails c' command.

Within the code, a lot of the work was done in db/migrate
and in app/models to customize devise for roles and users
and set up the appropriate user tables.  Additionally, there was a lot
of work to include the views in app/views/devise, especially
in the passwords, registrations and shared folders.

Some other work includes, but is not limited to, app/views/layouts
and app/views/welcome, the css work in the assets folder, and all
of the work that is found in the spec folder (I used rspec for
my tests), modifications to config elements and addition of
multiple gems.  Work was also done elsewhere to develop this filestructure
with the appropriate database, however most large parts have been
listed to improve review.


Author: Gabrielle Badie
