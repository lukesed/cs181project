class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :username
  # attr_accessible :title, :body
  
  # validates_uniqueness_of :username
  belongs_to :role
  before_create :set_default_role, :create_username

  # this sets the default username to be the email address
  def create_username
    self.username = self.email
  end

  private
  # this sets the default role to be 'student'
  def set_default_role
    self.role ||= Role.find_by_name('student')
  end
  
end