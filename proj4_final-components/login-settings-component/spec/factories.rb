Factory.define :user do |f|
  f.sequence(:email) { |n| "email@example.com" }
  f.password "secret"
end