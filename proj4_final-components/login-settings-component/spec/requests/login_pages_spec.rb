require 'spec_helper'

describe "logging in" do
  before (:each) do
    @user = Factory(:user)
    visit new_user_session_path
  end
  
  it "signs in user successfully with correct email and pw credentials" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => @user.password
    click_button "Sign in"
    page.should have_content 'Signed in successfully.'
  end
  
  it "redirects user to welcome page" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => @user.password
    click_button "Sign in"
    page.should have_content 'This is the sample welcome page.'
  end
  
  it "gives email error when email address field blank" do
    fill_in "Email", :with => ""
    fill_in "user_password", :with => @user.password
    click_button "Sign in"
    page.should have_content 'Please enter an email address'
  end
  
  it "gives input error when password field blank" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => ""
    click_button "Sign in"
    page.should have_content 'invalid email or password'
  end

  it "given input error for invalid email" do
    fill_in "Email", :with=> 'wrongEmail@example.com'
    fill_in "user_password", :with => @user.email
    click_button "Sign in"
    page.should have_content 'invalid email or password'
  end
  
  it "redirects user after already signed in" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => @user.password
    click_button "Sign in"
    
    visit new_user_session_path
    page.should have_content "You are already signed in."
    page.should have_content "This is the sample welcome page."
  end
  
  it "contains active forgotten password link" do
    page.should have_content "Forgot your password?"
    click_link "Forgot your password?"
    page.should have_content "Email"
    page.should have_content "Send me reset password instructions"
  end
end