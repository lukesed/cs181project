require 'spec_helper'

describe "updating user info" do
  before (:each) do
    @user = Factory(:user)
    visit edit_user_registration_path
    fill_in "Email", :with => @user.email
    fill_in "Password", :with => @user.password
    click_button "Sign in"
    
    visit edit_user_registration_path  
  end
  
  it "confirms form submission with correct credentials" do
    fill_in "Email", :with => @user.email
    fill_in "Current password", :with => @user.password
    click_button "Update"
    page.should have_content 'You updated your account successfully.'
  end
  
  it "does not confirm form submission with incorrect credentials" do
    fill_in "Email", :with => 'notemail'
    fill_in "Current password", :with => @user.password
    click_button "Update"    
    page.should have_no_content 'You updated your account successfully.'
  end

  it "displays account update with matched confirmation" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => "foobar"
    fill_in "Password confirmation", :with => "foobar"
    fill_in "Current password", :with => @user.password
    click_button "Update"    
    page.should have_content 'You updated your account successfully.'
  end

  it "does not accept password with mismatched confirmation" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => "foobar"
    fill_in "Password confirmation", :with => "barfoo"
    fill_in "Current password", :with => @user.password
    click_button "Update"    
    page.should have_no_content 'You updated your account successfully.'
  end

  it "does not accept password that is less than six characters" do
    fill_in "Email", :with => @user.email
    fill_in "user_password", :with => "foo"
    fill_in "Password confirmation", :with => "foo"
    fill_in "Current password", :with => @user.password
    click_button "Update"    
    page.should have_no_content 'You updated your account successfully.'
    page.should have_content 'Password is too short (minimum is 6 characters)'
  end
  
  it "gives email error message when email input is not a valid address" do
    fill_in "Email", :with => "foonoatsymbol"
    page.should have_content 'Please enter an email address'
  end
  
  it "updates username with correct credentials" do
    fill_in "Email", :with => @user.email
    fill_in "Current password", :with => @user.password
    fill_in "user_username", :with => "testname"
    click_button "Update"    
    page.should have_content 'You updated your account successfully.'
  end  
end
