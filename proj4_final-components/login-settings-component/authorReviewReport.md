## Author Code Review Report

##### by Gabrielle Badie


There weren't very many suggestions made for my component.  This made
it difficult for me to feel that the code review was helpful.  Some of
the code was not obvious to my teammates, partly was a result of
rookie knowledge of the ruby on rails framework, and unfamiliarity with the
devise gem.  As a result, the areas that I focused a lot of my
efforts on were not obvious to my teammates and required explanation.
Their comments were few, however I did not make changes to my code
that I would have not made otherwise, except adding some comments.
The only active suggestion was a 'comment' (not a must or should fix)
about making logins timeoutable.  I address this issue in my notes below.

What I will say, however, was that knowing my code would be reviewed by
my peers made me actively work to write better code
when I was programming, and make sure I was commenting appropriately.
The difference is analogous to the difference between doing assigned
reading for a class and doing assigned reading for a class that has a
daily quiz.  The latter usually provides a strong incentive to do a better
job with the assigned task.

I appreciated the process, although I feel
that this would have been more useful with a team more familiar to devise
and ruby on rails, or with a larger component that was more complicated
and susceptible to errors.  Perhaps being able to give a tutorial to devise
and the environment it creates is an example of something that is important
to a code review (providing context) and in that case this exercise was
helpful.

I do, however, feel that test driven development
would have been a better for this component as opposed to waiting until
after the review to run tests (as required).  Fortunately tests were automated
so results came quickly and did not warrant code changes.

## Author Notes From Meeting:

The forgotten password link was under app/views/devise/shared/_links.erb,
and this was clarified during the meeting.  When the component is viewed
in a standalone setting this becomes apparent, and was clarified during
testing.

I chose not to make the login timeoutable because it would add complexity
unnecessary to this component and was not a suggestion that came out in
the specifications or design processes.