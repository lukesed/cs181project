# Luke Sedney
# 12/11/2012
#
# This is a unit testing class for tex2png.rb that uses the test::unit testing
# framework. All the tests in each test case class share the same setup/teardown
# procedures. The test suite class runs all of the test case classes.
#
# There are some configuration requirements.
# 
# Tex2png must be located here with write and execute privileges:
# ./tex2png
#
# RubyGems must be installed, and the following gems must be available:
# test-unit
# image_size
# aws-s3
#
# All of the configuration requirements for tex2png.rb must, of course, also
# apply here.
# 

require "./tex2png.rb"
require "test-unit"
require "test/unit/ui/console/testrunner"
# used to get image dimensions
require "image_size"
# needed to make calls to the fake s3 server
require "aws/s3"

# Black box tests will all use the same setup and teardown procedure. This is
# automatically done before and after each test, so one test will not affect the
# others.
class TC_black_box < Test::Unit::TestCase

  def setup

    # connect to the fake s3 server
    AWS::S3::Base.establish_connection!( :access_key_id => "123",
       :secret_access_key => "abc", :server => "localhost", :port => "4567" )
       
  end

  def teardown
  
  end

  # This "test of tests" ensures that our image comparison methodology works.
  def test_image_equals
    
    # add two different images to s3 and then download them
    AWS::S3::S3Object.store('test.png', open('./static/invalid.png'), 'texImages')
    AWS::S3::S3Object.store('test2.png', open('./static/blank.png'), 'texImages')
    result = AWS::S3::S3Object.find( "test.png", "texImages" )
    result2 = AWS::S3::S3Object.find( "test2.png", "texImages" )
    
    # now read the first image directly from the file
    bytes = open('./static/invalid.png', "rb") { |file| file.read }
    
    # make sure the image from the file is the same as the s3 object
    assert_equal(result.value, bytes)
    
    # and make sure the different image is not considered equal
    assert(result2.value != bytes)
  end

  # Call newImage with a valid tex expression that includes symbols from the
  # amsmath package. Test is successful if an S3 key to an image showing that
  # expression is returned.
  def test_normal

    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image(" test $\sum x_n^2$ "),
                                     "texImages" )
    # get raw image data
    image = ImageSize.new(result.value)
    # checking whether the expression is correct would require manual review,
    # but we do a sanity check on dimensions
    image = ImageSize.new(result.value)
    assert( image.h > 5 && image.w > 5 )

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image(" test $\sum x_n^2$ ", "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    image = ImageSize.new(result.value)
    assert( image.h > 5 && image.w > 5 )

  end

  # Call newImage with an empty string. A blank image error should be returned.
  def test_empty_string


    # load blank image error file for comparison
    blank = open('./static/blank.png', "rb") { |file| file.read }
    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image(""), "texImages" )
    # make sure image produced by function is error image
    assert_equal(result.value, blank)

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image("", "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    assert_equal(result.value, blank)


  end

  # Call newImage with a valid tex string that does not produce output. Again, a
  # blank image error should be returned.
  def test_blank_output

    # load blank image file for comparison
    blank = open('./static/blank.png', "rb") { |file| file.read }
    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image("{ }^{ }"), "texImages" )
    # make sure image produced by function is error image
    assert_equal(result.value, blank)

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image("{ }^{ }", "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    assert_equal(result.value, blank)

  end

  # This describes the next three tests:
  # Call newImage with input producing images with the following dimensions:
  # 1. Width > 640, height <= 480.
  # 2. Width <= 640, height > 480.
  # 3. Width > 640, height > 480.
  # A URL to an image stating that the image is too wide and/or high should be
  # returned.
  def test_too_wide

    # load dimension error image file for comparison
    dimension = open('./static/dimensions.png', "rb") { |file| file.read }
    # make a string that will create an overly wide image.
    input = "the_underscores_will_force_this_to_stay_on_one_line"
    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image(input), "texImages")
    # make sure image produced by function is error image
    assert_equal(result.value, dimension)

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image(input, "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    assert_equal(result.value, dimension)

  end

  def test_too_tall

    # load dimension error image file for comparison
    dimension = open('./static/dimensions.png', "rb") { |file| file.read }
    # make a string that will create an overly tall image.
    input = "this \newline will \newline be \newline too \newline tall \newline 
             due \newline to \newline all \newline the \newline newlines"
    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image(input), "texImages")
    # make sure image produced by function is error image
    assert_equal(result.value, dimension)

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image(input, "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    assert_equal(result.value, dimension)


  end

  def test_too_large
    
    # load dimension error image file for comparison
    dimension = open('./static/dimensions.png', "rb") { |file| file.read }
    # make a string that will create an overly wide and tall image.
    input = "forcethistostayononeline and this \newline will \newline be \newline too \newline tall \newline due \newline to \newline nls"
    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image(input), "texImages")
    # make sure image produced by function is error image
    assert_equal(result.value, dimension)

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image(input, "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    assert_equal(result.value, dimension)

  end


# Call newImage with invalid TeX.
# A URL to an image stating that the code could
# not be parsed, and that only amsmath is available should be returned.

  def test_invalid_tex

    # load invalid error file for comparison
    invalid = open('./static/invalid.png', "rb") { |file| file.read }
    # get S3 value of key returned by new_image
    result = AWS::S3::S3Object.find( Tex2png.new_image("{\invalid"), "texImages" )
    # make sure image produced by function is error image
    assert_equal(result.value, invalid)

    # do same test test with optional key parameter

    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image("{\invalid", "testKey"), "testKey" )
    # get the S3 value for the key
    result = AWS::S3::S3Object.find( "testKey", "texImages" )
    # proceed as above
    assert_equal(result.value, invalid)

  end

end

# White box test:
# We test Tex2png failure case. We cause the call to fail by renaming the
# application with a system call. We then run new_image with one valid tex
# string and one valid with and without the optional parameter. This should
# produce  two entries in the texFailedQueue, both with S3 keys which point to
# "cannot process" images. We then use another system call to re-enable tex2png,
# and execute processTeXQueue. After this call, all of the images should display
# as described in the black box section.

class TC_white_box < Test::Unit::TestCase

  def setup
    # connect to the fake s3 server
    AWS::S3::Base.establish_connection!( :access_key_id => "123",
       :secret_access_key => "abc", :server => "localhost", :port => "4567" )
  end

  def teardown
    # this moves tex2png back in case the test had an error partway through
    system("mv tex2png.renamed tex2png 2>/dev/null")

  end

  def test_tex2png_down
    
    # rename tex2png so it cannot be used
    `mv tex2png tex2png.renamed`
        
    # get S3 value of key valid tex with and without parameters.
    result_valid_key = Tex2png.new_image(" test $\sum x_n^2$ ")
    result_valid = AWS::S3::S3Object.find( result_valid_key, "texImages" )
    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image(" test $\sum x_n^2$ ", "testValid"), "testValid" )
    # get the S3 value for the key
    result_valid_param = AWS::S3::S3Object.find( "testValid", "texImages" )
    # do the same for invalid tex code
    result_invalid_key = Tex2png.new_image( "{\invalid")
    result_invalid = AWS::S3::S3Object.find( result_invalid_key, "texImages" )
    # ensure that the key in the parameter is returned
    assert_equal( Tex2png.new_image( "{\invalid", "testInvalid"), "testInvalid" )
    # get the S3 value for the key
    result_invalid_param = AWS::S3::S3Object.find( "testInvalid", "texImages" )    
    # load the "could not process image" for comparison
    process = open('./static/process.png', "rb") { |file| file.read }
    
    # make sure the error image is shown
    assert_equal(result_valid.value, process)
    assert_equal(result_invalid.value, process)    
    assert_equal(result_valid_param.value, process)    
    assert_equal(result_invalid_param.value, process)
    
    # put tex2png back so it can be used again
    `mv tex2png.renamed tex2png`
    
    # now procss the queue of failed requests
    Tex2png.process_tex_queue()   
    
    # reload the images from s3
    result_valid = AWS::S3::S3Object.find( result_valid_key, "texImages" )
    result_valid_param = AWS::S3::S3Object.find( "testValid", "texImages" )
    result_invalid = AWS::S3::S3Object.find( result_invalid_key, "texImages" )
    result_invalid_param = AWS::S3::S3Object.find( "testInvalid", "texImages" )
    
    # checking whether the expression is correct would require manual review,
    # but we do a sanity check on dimensions
    image_valid = ImageSize.new(result_valid.value) 
    image_valid_param = ImageSize.new(result_valid_param.value)
    assert( image_valid.height() >5 && image_valid.width() >5 )
    assert( image_valid_param.height() >5 && image_valid_param.width() >5 )
    assert( image_valid.h > 5 && image_valid.w > 5 )
    assert( image_valid_param.h > 5 && image_valid_param.w > 5 )

    # load invalid error image for comparison
    invalid = open('./static/invalid.png', "rb") { |file| file.read }
    # check if the error image is displayed 
    assert_equal(result_invalid.value, invalid)
    assert_equal(result_invalid_param.value, invalid)
    
  end

end

# The test suite class runs both of the test case classes.
class TS_tex2png < Test::Unit::TestSuite
  def self.suite 
   suite = Test::Unit::TestSuite.new
   suite << TC_black_box.suite
   suite << TC_white_box.suite
   return suite
  end
end

# run the test suite
Test::Unit::UI::Console::TestRunner.run(TS_tex2png)
