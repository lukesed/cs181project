# Luke Sedney  
# 12/15/2012
#
# Tex2png is a Unix utility that takes TeX code and returns PNG images of the
# expressions that the code specifies. We require it so we can allow users to
# embed LaTeX in their posts. This component consists scripts the operation of
# tex2png. When it receives a post with LaTeX, the Rails controller responsible
# for adding messages will call this code to render the images, then embed the
# images in the post at the point where the code was entered.
#
# There are some configuration requirements.
#
# The libaray pnmtopng must be available. In unix systems, it is provided by 
# the common netpbm package.
# 
# Tex2png must be located here with execute privileges:
#
# ./tex2png
# 
# The directory ./output must exist.
#
# The source for Tex2png can be found here:
# http://www.nawouak.net/informatics/tex2png/code/tex2png.htm 
# Copy the code to a file tex2png.c, and it can be built with 
# "g++ tex2png.c -o tex2png".
#
# The following files must be present in ./static:
# invalid.png
# blank.png
# dimensions.png
# process.png
#
# A Fake S3 server must be running on localhost post 4567, with the default key
# id "123" and secret key "abc". A real S3 server could be substituted if the
# parameters to the establish_connection call were modified appropriately.
# Fake S3 can be started with this command:
# "fakes3 -r ./fakes3_root -p 4567"
# 
# The easiest way to get Fake S3 is by installing the fake-s3 gem, but the gem
# is not required - the server just needs to be running.
# 
# the following gems must be installed:
# aws-s3
# image_size
# 
# If you plan to run test_tex2png, the gem test-unit is also required, and
# ./tex2png must be writable as well.
# 

# talk to the s3 server
require "aws/s3"
# used to get image dimensions
require "image_size"
# used to place a limit on waiting for system call
require "timeout"
# needed to get system io
require "open3"
# needed for queue
require "thread"

class Tex2png 

  # A queue of elements to be processed in the event that TeX2PNG fails. Each
  # element of this queue will include the code to be processed, and the key on
  # S3 where the dummy image has been stored.
  @texFailedQueue = Queue.new

  # connect to s3 server
  AWS::S3::Base.establish_connection!( :access_key_id => "123", 
    :secret_access_key => "abc", :server => "localhost", :port => "4567" )

  # This function takes tex code as a parameter and an optional key string.  If
  # there is no key, it generates a unique one. If the caller provides a key, 
  # any existing objects with that key will be overwritten.
  #
  # It calls the tex2png utility to convert the file, with parameters
  # instructing it to put the output in "./output" in the correct format.
  # The s3 key is used as the output file name
  # 
  # If any of these problems occur, and error image stating this is used:
  # Image is not within the dimensions 640x480.
  # tex2png produces a syntax error. 
  # The the input string is empty or the resulting image is blank (this causes
  # an error in tex2png).
  # If the call to tex2png otherwise fails.
  #
  # In the last case, the input code and the S3 key are added to texFailedQueue.
  # In the production version, whatever error logging system we use would be
  # notified.
  #
  # Finally, the function calls imageToS3 with the location of the image file
  # and the key, and the input code file is deleted.
  # 
  # It returns the S3 key.
  def self.new_image(code, optKey=nil)

    # set key to optional parameter
    key = optKey

    # if optional parameter is blank, need to generate a unique key
    if optKey==nil

      # start with the current epoch time
      time = Time.now.to_i.to_s
      key = time

      # append a counter to time, and iterate until key is unique
      counter = 1
      while AWS::S3::S3Object.exists?(key, "texImages")
        key = time + counter
        counter+=1
      end

      # add file extension
      key += '.png'

    end

    # create file path
    file = './output/' + key

    # if input is blank, use blank error image.
    # will cause command argument syntax error if given to tex2png.
    if (code == '')

      `cp ./static/blank.png #{file}`

    else

      # attempt to convert code to image
      
      begin
        # use a timeout in case calling tex2png takes too long
        status = Timeout::timeout(20){
          # call tex2png and, and get output io stream
          Open3.popen2e('./tex2png', "-v", code, file) {|inp, out, wait|

            # read each line of output
            out.each {|line|

              # happens if syntax error
              if line.include?("! ")
                `cp ./static/invalid.png #{file}`
                break
              end

              # happens if output blank.
              # this is the minimal file with no symbols.
              if line.include?("written on tex2png.dvi (1 page, 152 bytes)")
                `cp ./static/blank.png #{file}`
                break
              end

            }

            # close the streams
            inp.close
            out.close
          }
        } 

      rescue

        # case of tex2png timeout or failure
        `cp ./static/process.png #{file}`
        # add to queue of failed items
        @texFailedQueue << {"code"=>code, "key"=>key}

      end

    end

    # load image file to check dimensions
    image = open(file, "rb") { |file| file.read }
    size = ImageSize.new(image) 
    # if image is too large, use dimensions error file
    if( size.h > 480 || size.w > 640 )
      `cp ./static/dimensions.png #{file}`
    end

    # put image on s3
    image_to_s3(file, key)

    return key

  end

  # The image at file is uploaded to S3 with key, and file is deleted.
  def self.image_to_s3(file, key)

    # put object in server
    AWS::S3::S3Object.store(key, open(file), 'texImages')

    # delete the file
    system("rm #{file}")

  end

  # This function should be run by a system administration when tex2png
  # functionality is restored after a failure. It calls new_image on each
  # element with the image and key stored for each item in the queue.
  def self.process_tex_queue
    # loop when the queue is not empty
    while !@texFailedQueue.empty?

      # call new_image on each entry
      entry = @texFailedQueue.pop
      new_image(entry["code"], entry["key"])

    end

  end

end