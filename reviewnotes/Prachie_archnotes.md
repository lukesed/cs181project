Main Issues
===========
* Security of user information and tables
* No notes on scalability; I know nothing about Django but that doesn't mean its okay
* Missing and unfinished sections--suggestions for improvement at end of document


Going through Architecture part by Part:
========================================

Introduction
------------
* Wish they had a quick intro into what their project actually was

1.1 Architecture of Navigation
-------------------------------
* Team hasn't considered settings,password changing, etc options in navigation that 
users would need to see

2. Architecture from Components Perspective
--------------------------------------------
* templetes must be stable and unchanging (right? If not, need to clarify), so I'm 
not sure if Database.Results.html won't have to change
* Is there security for UserSetTable? 
* NO talk of security for account information, or even where that info will be stored

2.3 Persistent Data Objects
------------------------
* Please be more specific. All I got from that was "there are the persistent data objects 
we need to make the things happen". How much storage are you expecting at first, how is 
that scalable, etc
* In fact, how is anything scalable? No evidence that they've thought about that, especially
for adding new cards to decks

2.5 Key Issues
----------
I think this wasn't finished, ended mid-sentence
"Our architecture helped achieve this in two ways. First, by using " was the end

Also
-----
* Where are group lists stored?
* Will all editing be done using forms?
* Prototype and components analysis missing; hard to comment on the component quality
because no arguements were made.
* More use cases, and actors that will use product
