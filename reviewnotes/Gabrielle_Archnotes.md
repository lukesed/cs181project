Architecture Review - Individual Notes
---------------------------------------

<dl>
  <dt>Introduction</dt>
  <dd>
      What is this project about?  Although I was introduced
      to the architecture, I would have hoped to be introduced to the product
      first and the goals of the team</dd>
  <dt>1.3 Interfaces</dt>
  <dd>
    "As described above, most of the interfaces between the components are
    hyperlinks. However, additional advanced HTML5 features will be required
    to support interactive functionality." --> Advanced features, how?
    Interactive functionality is vital to a system which requires the user
    to interact constantly with the interface (i.e. with flashcards).
    Slightly more depth needed here.  Prototype??
    </dd>
  <dt>2.1 Components</dt>
  <dd>Web hosting service for Django?</dd>
  <dt>2.2 Persistent Data Objects</dt>
  <dd>This section seems incomplete.  'This data is
    stored on a server' is vague and needs clarification (see previous
    comment), and although 'commands' are listed above, this
    does not describe further how data is handled from
    input from the user to storage of data. </dd>
  <dt>2.6 Major Outstanding Issues</dt>
  <dd>Good to recognize that lack of familiarity with
  the Django web framework is an issue, although a link to
  tutorials isn't too much reassurance.</dd>
  <dt>3.1 Operations by Webpage</dt>
  <dd>Web hosting service for Django?  What is physically
  hosting your website.</dd>
  <dt>Use Cases</dt>
  <dd>In the inerest of the user, I would have definitely
  expected and wanted to see use cases here, not just
  navigation paths.  For example a diagram showing
  potential goals someone wants to accomplish in a site visit and
  what steps would need be taken in that session.</dd>
  <dt>Classification of Users</dt>
  <dd>How are you classifying your users?  Are they students
  looking to make flashcards for a class or adults
  trying to learn new vocabulary words in a second language?
  Perhaps there is already a specific audience for Django and
  this is unclear because I do not have a clear understanding
  of the project (see comment on introduction).</dd>
  <dt>Webpage navigation</dt>
  <dd>Good outline of webpage types and navigation,
  although we have to be sure of users navigating
  outside of the bounds listed here.  What if a user
  wants to save the URL to access a specific set of cards
  as a 'favorite' in his/her browser?  Where does the user
  land?</dd>
  <dt>Safety and Security</dt>
  <dd>How will usernames/passwords be safe and secure,
  but, further, how will personal information (flashcards,
  etc.) be secure?  This topic isn't approached.</dd>
  <dt>Scalability</dt>
  <dd>Adding new decks, new cards, hundreds or thousands
    of new users?  (or more) This subject needs to be approached
    in this architecture.</dd>
  <dt></dt>
</dl>