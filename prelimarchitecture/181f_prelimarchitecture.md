Introduction
============

Purpose 
-------

This document provides a comprehensive architectural overview of the system,
using a number of different architectural views to depict different aspects of
the system. It is intended to capture and convey the significant architectural
decisions which have been made on the system. Here, we outline the use cases
supported by the system and the architectural styles and components that have
been selected to best achieve the use cases.

Scope 
-----

This software architecture document provides an architectural overview of our
Q&A website and web service, QED. This website is being developed by team
TheTeam to support question/answer processes and after-class communication
between students, professors and teaching assistants.

Architecture Overview
---------------------

Our product, QED, is a web application. The back-end runs on an Amazon Elastic
Cloud Compute (EC2) instance. Ruby on Rails will be the application framework.

Requests from user browsers are received by Apache server, which communicates
the request to Rails. Rails generates the required content to send back in
response, and performs any other actions necessitated by the application logic.

Rails will store most information in an SQLite database, which will need to be
queried and updated frequently. It will also store larger assets like documents
and images in an Amazon Simple Storage Service (S3) store. When a user requests
a page requiring database information, Rails queries the database while
generating the page. When an file stored on S3 is required, Rails requests that
asset and returns it along with the page.

Tex code will be converted to image files by the TeX2PNG program. After the 
image files are created, Rails will move them from local storage to S3.

![alt text](https://raw.github.com/Lukesed/cs181project/master/prelimarchitecture/components.png "Component Diagram")

This diagram shows the components of the QED system. The runtime components are
those contained within the EC2 instance: Apache, Rails, TeX2PNG, and SQLite.

Courses are not deleted from the system after the semester ends - the course
page is left up, so students can continue to use it as a resource.

Use-Case View 
=============

This view presents the users perception of the functionality provided by the
web applications of the QED technical standards. These use cases were
synthesized from user collaborations and requirements elicitations but may not
include all of the descriptive text of these interactions. However, we hope to
traverse the most important action items of the users to ensure their needs
will be met by our architecture.

Actors 
------

The following is the list of known actors that will interact with the system.
The actors are defined by distinct email address usernames.

Note that, for each course, the student can be in three different states:
Current Student, Former Student, and Teaching Assistant These will each have
different permissions as follows:

* Current Student: This user state has permissions to ask questions, answer
questions, manage study groups for that course, and upload/download documents.
* Teaching Assistants: This user state has permissions to answer
questions, manage study groups for that course, manage enrollment, and
upload/download documents.
* Former Students: This user state has permissions to answer
questions and upload/download documents.

Instructors are separate set of users, and cannot be students in a course Their
permissions are to: make announcements, ask questions, answer questions,
upload/download documents, and manage enrollment.

Specifically, the manage student list function allows for removing and inviting
students, former students, and TAs.

The manage study groups function allows for creating groups, inviting others to
groups, joining groups one is invited to, sending posting to groups, and
receiving and viewing posts from the group members.

These specifications are necessary for the following use cases.

![alt text](https://raw.github.com/Lukesed/cs181project/master/prelimarchitecture/useCase.png "Use Case Diagram")

* Login: This use case allows students and professors to login to their own
accounts using their email and password. This login screen is the first screen
visited on the site for the actors. Once they login they can pick a course that
they are enrolled in. Anyone can be an actor here.

* Manage class roster and enrollment: This use case allows instructors to manage
(ie. invite, ban) students in specific classes. These functions are accessible
via an additional instructor settings pane. When actions are taken by the actor
to invite or ban a student, the browser sends this instruction to the system.
Rails updates the database and send invitation emails to users when appropriate.
Professors are the only actor here.

* Make announcements: This use case allows actors to post an announcement for a
specific course. When the announcement is written and sent, the browser sends
it to Rails, which updates the database. A professor can be an actor here.

* Ask question or Upload Document: This use case allows actors to post a
question and attach documents to a post. The user may select a file to be
uploaded from their computer. When the post is written and sent, the browser
sends it to Rails, which updates the database and S3 server if necessary.
Anyone can be an actor here except a former student or a teaching assistant.

* Respond to post: This use case allows actors to answer or comment on a
question and upload documents. The user may select a file to be uploaded from
their computer. When the post is written and sent, the browser sends it to
Rails, which updates the database and S3 server if necessary. Anyone can be an
actor here.

* Manage study groups: This use case is split into different parts. All of these
can be done from the settings button for each user, specific to the course
page.

1. Post to group: This use case allows actors to post a message to a study group
(not to specific students). When the post is written and sent, the  browser
sends it to Rails, which updates the database. Only a student can be an actor
here.
2. Create a group: This use case allows students to create a study group. When
this option is chosen, the browser informs Rails, which updates the database.
Only a student can be an actor here.
3. Invite to group: This use case allows students to invite other students to
their study groups. The browser sends this instruction to the system. Rails
updates the database and sends invitation emails to users when appropriate.

* Manage account settings: This use case is includes changing passwords and
display names. The user changes their information using a form in the settings.
The browser sends it to Rails, which updates the database and S3 server if
necessary. Anyone can be an actor here.

Interfaces 
==========

External
---------

SSH Access:
----------
SSH connection to the EC2 instance will be used by business and sales employees
when adding, deleting and editing classes' and instructors' entries in the
databases. This interface would additionally be used for bug fixes, backups, and
any other maintenance tasks. All of this would be done through the built-in CLI
of the runtime components.

Since new instructor accounts are created only by sales staff, we feel that it
is appropriate to move issues of payment and administration outside the scope
of our architecture. 

Web Interface: 
--------------
This is the interface that is shown to users, allowing them to easily and
clearly interact and use our system. This interface is reasonably simple and as
an html page will have the flexibility to adapt to changes in the website,
including the potential addition of new features.  The user interface prototype
linked below can be reasonably programmed and updated as needed.

There shall exist three webpages: the Log In page, the Q&A page, and the Settings
page. In the Log In page, if a visitor inputs a valid username and password the 
visitor is taken to the Q&A webpage (via hyperlink), where he or she may now 
post a question, etc. The Q&A webpage shall have the same view for every class a 
user is enrolled in. The information (posts, answers, and such) that is shown
is updated on the Q&A webpage by Ruby bringing forth different information from 
the database. The Settings page, which may be accessed via a hyperlink on the 
Q&A webpage, has two functionalities: allow a user to change his or her password, 
and allow a user to change his or her display name. Notice that a user clicking 
on the "Log Out" button shall simply end the QED session and redirect the user 
to the Log In webpage. The students can change which course they are looking at
by scrolling down the list of their courses on the main navigation bar (see our 
prototype for details). The study groups use the same webpage as the courses, and 
a user can move through study groups by usingthe drop down for study groups on 
the main navigation bar.

If a user is not logged into the QED website, any attempt to enter any of the 
three webpages mentioned above causes the user to automatically be redirected to 
the Log In page. Thus, not allowing bookmarking into Q&A webpage or Settings page
unless signed in.

Email:
-----
The Rails component Active Mailer sends email to users.


Internal
---------

HTTPS
-----
The user's browser accesses the Apache server via an HTTPS connection. 

Passenger
---------
Passenger is a package for Apache that handles the integration of Apache and
Rails. It passes user requests on to Rails.

Sqlite3-ruby 
------------
The Rails package sqlite3-ruby provides integration with the SQLite process.

AWS SDK for Ruby
----------------
The AWS SDK Rails package gives Rails access to AWS services, including S3.

Process::exec
-------------
This is a Rails function used to execute another program on the system. This
would be used to run TeX2PNG, instructing it with command line arguments to
convert tex files to images.

File system access
------------------
Each of the components on the EC2 instance make use of the instance's local file
system, in the same way that processes running on a machine normally do.

Prototype
=========

Our goal in creating this prototype was to explore the usability of the UI. 
Our were happy with our UI "in theory", but wanted to ensure that it was
practical by creating a mock-up. This mitigates the risk that we could
discover that our product is not usable after expending significant 
resources creating it.

*[Click here ](https://github.com/Lukesed/cs181project/blob/master/prelimarchitecture/mockup.png)
to view a prototype of the user interface.*

After using the prototype, we feel confident that our UI is cohesive and that 
users will find the functionality intuitive. We also discovered that our mental 
images of the UI differed slightly, so producing a prototype had valuable side-
effect of ensuring that we all had the same product in mind.


Component Analysis 
==================

SQLite 
------ 

SQLite is a relational database management system that we will use to store,
organize and query our data. We choose SQLite because it takes up little memory,
is open source (and therefore free), and has good performance. Our schemas are
detailed later in our architecture. Our SQL database is going to be stored on
the instance. We are confident that the 160 GB default EC2 instance storage is
enough for our initial launch.

Amazon S3
-------------

The documents and images mentioned will be stored in the online storage web
service Amazon S3. S3 can store very large quantities of data at relatively low
cost, and Amazon guarantees 99.9% uptime. There is also a theoretical
performance benefit to using a storage system that is part of the same
infrastructure as the EC2 instance the system runs on. Pricing is based on
storage and bandwidth consumption. It seems very unlikely that our data needs
would reach the point where maintaining our own datacenter would be cost-
effective.

Ruby on Rails
--------------
Ruby on Rails is a web application framework. It supports a great deal of
functionality immediately, and extensions that add more are available for easy
installation. Since our group is little experience in web development, using
Rails will reduce the number of components that we need to become familiar with
in order to work on our product. In addition, Rails is used by a great deal of
sites, including those run by major companies. We have more confidence in this
stability of such a system than one that we built from multiple components.

Rails uses the common Model-view-controller architecture. Each web page 
(detailed elsewhere) corresponds to a view. When a page is requested, the
controller pulls data from the models and synthesizes the page from the
appropriate view. The models interface with the SQLite3 database, updating
and querying data when necessary. The models, views, and controllers are all
subcomponents within the Rails process.

Additional Runtime Components
=============================



Persistent Objects
==================

One set of persistent objects are our database entries in the SQLite database.
We chose SQLite as our database because it is fast, easy to use, and free. The
entries are all simple strings and integers that come from time, passwords,
usernames, posts, announcements, etc. In other words, this database will hold
much of the content of our website as well as other critical entries. The
information will be organized into the following categories: Questions,
Announcements, Responses, Username and Passwords, Messagint), class roster and
permissions. Note that courses are not deleted once they are over, new courses 
are just added.They will be organized as follows:

Students have attributes: keys of courses user is a student or TA in (int), 
list of study group key (int), email (string), password hash (string), 
university/consortium key (int), display name, and user key.

Instructors have attributes: keys of courses user is an instructor in (int), 
email (string), password hash (string), and university/consortium key (int),
display name, and user key.

Questions have attributes: key (integer), time (integer), user key (string), 
question content (string), keys of any documents or images (integer), keys of 
responses, and user display name.

Announcements will have the same attributes.

Responses (ie comments and answers) have attributes: key (integer), question or
announcement's key (integer), time (integer), user key (string), response
content (string), keys of any documents or images (integer), and display
name.

Documents/images have attributes: key (integer), key of post (integer), and
S3 address (string).

Study groups will have a key (integer), keys of posts made to group, and a list
of user keys that are in the study group.

Post in study group have attributes: key (integer), time (integer), user key
(int), study group key (integer), message (string), and display name.

Courses have attributes: key (integer), separate columns for keys of students,
former students, teaching assistants, and instructors (int), keys of questions
and announcements (int), course title (string), and university/consortium key 
(int).

The documents, images, etc that are uploaded by users will be stored on Amazon
S3. See the component analysis for more information on why we decided to use
S3. The use of S3 with Rails is enabled by the AWS SDK for Ruby.


Authentication and Authorization 
--------------------------------

One of the most significant risks for any web application is a breach in the
security of user data. For this reason, we are choosing to utilize the commonly
used Devise package for Rails. This package handles user registration, log in,
cookies, and password reset. Using a popular open source solution should
significantly reduce the incidence of security flaws in the implementation.
Devise uses industry-standard salting and hashing, so even if the database is
compromised, passwords will not be lost.

Devise Invitable is a package for Rails systems with Devise that allows user
registration via email invitations. This will be the only method of registration
for students.

Sales and support employees will connect to the EC2 instance via SSH to add
database entries for new customers. This is how instructor accounts and courses
are added. This moves payment/invoicing and the many accompanying security risks
outside the scope of our system. After an instructor account is created by
staff, the user can only log in after requesting a password reset email. This
ensures that passwords can only be created by user interaction with the Devise
system, so our employees are not responsible for passwords. This CLI access
could also be used to regularly backup the database and S3 store.

Users (browsers) connect to the Apache server via HTTPS, avoiding risk of 
man-in-the-middle attacks.

Scalability 
-----------

While we feel that the actual scaled system is outside the scope of our first
release, we did make some choices that should simplify that work if and when it
needs to be done.

If the load on our EC2 instance becomes too great, we can at first simply
upgrade to a more powerful instance. This would require little change in
configuration. If this is not sufficient, we could expand to multiple instances.

Each user and course is bound to a single university/consortium ID. By setting
the constraint that items with the same ID are served by the same instance, we
could keep entirely separate databases in each instance. This would avoid the
problem of ensuring consistency across instances.

If local instance storage becomes an issue before CPU load, we could move the
database to Amazon Elastic Block Storage (EBS).

Implementation Risks 
----------------------- 

When implementing the described architecture above, a few risks exist that may
impede us from adequately completing our website by a certain time period.

The first risk concerns us, the developers, taking too long to become familiar
with coding in Ruby and web languages. A related issue the process of developing
the system before deploying it. Apache, Rails, and TeX2PNG can all be run on a
developer's computer. The same is not true of of Amazon S3. However, there is a
rails package called Fake S3 that was built to solve this problem. Fake S3
responds to the same calls as Amazon S3, but lives on the local machine.

Another risk that potentially may impede implementation is that some of the
programs and ruby packages we plan to use, Devise Invitable and TeX2PNG, are
uncommon and relatively new. TeX2PNG even requires that LaTeX be running on our
Amazon EC2 servers. Although installing this software should be no problem, some
unknown issues may arise when it is run in the context of an EC2 instance. This
could prevent us from implementing our architecture as planned. We can address
this risk by testing TeX2PNG on an EC2 instance before the rest of system is 
deployed.
