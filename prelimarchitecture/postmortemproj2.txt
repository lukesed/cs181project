Post Mortem, Project 2


Altogether, this project was stressful for the group, but we are proud of the results that our work has brought forward.
It was difficult to schedule meetings and work together because of conferences, breaks, etc where grup members were out
of town. However, we were able to stay on the same page by using the communication rules that we discussed in the last
project's post-mortem.

The most challenging aspect for us, and thus the greatest opportunity to learn, was in creating the initial architecture.
We did not realize the extent of the architecture and therefore did not realize, in the management plan, that it would
really be need to be worked on by every member of the group. To fix a similar problem fr next time, we should realize
when we don't understand our expectations for a section and take action accordingly, by emailing or meeting with
Professor Kampe to ensure we understand the time and effort needed for each part of the project. These expectations
should be crystal clear during the management plan stage so that we allow enough time for each part of the project.

Once we started working on the initital architecture, it became clear that this part of the project would take more time
and group effort than was planned. Looking back, we are happy with the way we handled the situation. We held a group
meeting and decided to seriously change our management plan by giving ourselves more time for the preliminary 
architecture, so we would have a better architecture going into the review. We did the component design analyses within
this preliminary architecture and initially refined the achitecture multiple times before the review. We are happy with
this investment at the beginning of the project because we feel that it significantly increased the quality of our final
architecture.

One problem with this approach was that many deadlines after the preliminary architecture became tight. For example, the
review, final architecture and post-mortem were all due within a 72 hour period. Because we put significant investment
into the preliminary architecture, the edits to the final architecture were not time-intensive, so this was acceptable.
However, the added stress could have been avoided by planning ahead at the very beginning of the project and giving
ourselves enough time for the preliminary architecture by starting the architecture earlier. In the future, we will try
to fix similar issues by making our draft deadlines significantly earlier than our final deadlines, so we encouraged to
start earlier and thus have more time to fix misunderstood expectations. Further, once we had pushed the deadline of the
preliminary architecture once, the deadline kept having to be pushed because we were on Fall Break and students were not
in the same country, much less same school. Communication was thus difficult. In the future, the next time we have such
an issue we should set up a time to skype that is convenient for everyone before everyone parts ways.

Each member of our group studied for the design review individually, and then we met up as a group to go over our major
issues. We are glad we did that because everyone had different things to bring to the table. We organized our issues by
section. This helped streamline the review process but made it more difficult to decide what was a must fix, should fix,
and nice-to-fix item since we did not organize by priority, but rather by section. In the future, we should do both
organizations so we know both the section-by-section organization of the review, and also have an idea of the items in
terms of priority. This would have helped better organize the design review meeting when we were reviewers. The design
review meeting as reviewees was helpful in understanding how an outside party would read our architecture.

Gabrielle wrote the review report using our feedback and asked us for feedback before turning it in. Having everyone
involved ensured we gave the other team comments that were properly organized and prioritized. We revised the 
architecture as a team, dividing up changed using our own notes from the review (We didn't get their review notes until
there wan't enough time to use them, again underscoring the issues noted earlier with tight deadlines after the
preliminary architecture). These notes are on GitHub.

Altogether, our team is pleased with our work and are glad we used our suggestions from the last project to ensure good
communication in this project. However, we have new lessons to learn. The most important lesson is to make ure we
understand project expectations and scope before creating the management plan so we don't have to change deadlines later
on as much, and we can be more flexible between draft and final deadlines.