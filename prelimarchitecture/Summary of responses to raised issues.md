Summary of responses to raised issues
=====================================

Component Analysis
------------------
We have expanded our discussion of Ruby on Rails, to better explain how it works and what its scope is in our project.

Actors
-------
We've clarified the former students section by making it clear that former students cannot ask questions. We have also explained the display name characteristic of users, and that users' usernames are their emails.

Use cases
----------
We have added a use case, in both the text and the diagram, pertaining to users' ability to change passwords and display names. We have also clarified that discussion in study groups and within the group, not between two specific users.

CLI to SSH Access
------------------
We have renames and expanded the CLI section to an SSH Access section to explain the connection and how the editing of the database will work using the CLI.

Web Interface
--------------
We have added a section that lists the web pages used in navigation and explain their functions and how to navigate between them.

File System Access
--------------------
We have clarified that the file system accessed is only the instance of EC2's file system, not the end user's file system..

Persistent Objects
---------------------
We have fixed our schemas so our objects are doubly linked and consistent. We have also noted that courses will not be deleted and will be considered persistent objects even after the course is over. 

Authentication and Authorization
---------------------------------
We have expanded this section to include a section on Devise, to explain what it is and its scope in our project. We explain how users can select their own passwords.

Scalability
-------------
We have expanded Scalability section to better explain how the system would be scaled. We already had written how we would separate data onto different machines but attempted to make this clearer.

Implementation Risks
---------------------
We discussed how Amazon EC2 and S3 would be simulated during product testing.

Runtime Components
--------------------
We have explicitly stated when a component is a runtime component.

Other notes:
-------------
The HTTPS Connection budgetary details, considered a 'should fix', has not been included because we decided it was outside the scope of the architecture. It would definitely be discussed in a budget proposal. We have also not included Javascript/CSS discussions in the final architecture, as Professor Kampe said we could consider it in implementation detail, so it is outside the scope. 



