Prachie Banthia

The Feed on the main Q&A page of the QED Website:

Requests to see the classes and study groups of a specific student will
populate the main Feed (which was picturized as the left column in our prototype in the
final architecture). Once a user clicks on an entry in the main Feed, this
will be displayed in more detail in the Viewer (which was picturized as the 
right column of the prototype in the final architecture).  The component allows students
to view, in reverse chronological order, the posts in a specific class. Also, it displays
the options to edit and delete posts, and create a new post. The back-end functionality
for the delete action is in this component, but does not include the back-end for the edit
and create post actions. This component�s role in the system is critical. This component 
allows the user to view different information that they are privy to. This component is
centered, therefore, on the interaction between the user, the web interface, 
and the Apache server. At the very top of the Feed, there will be a entry for Announcements.
In the final product, when the user clicks on this entry, a list of recent announcements will
populate the Viewer. For the Feed component, we need a Announcements clickable entry that will
integrate with the Viewer.

There are two external interfaces that interact with this component. The 
is the user using the browser by clicking on toolbar items to return information
in the Feed or clicking on a Feed entry to view in the Viewer. The second is 
the Apache server interface, as this component will request information to 
populate the Feed. This component integrates closely with other components. One
noteworthy integration is with the Post Management system, which edits and creates new posts.

Requirements:

* The Feed displays only root posts, not the content of the posts 
* Each Feed entry should have a Edit and Delet button
* The Feed displays at most the 10 most current entries
* This population must happen practically instantaneously.
* The first entry in the Feed must be an Announcements entry, which will show the most
recent announcement
* The Feed displays posts in reverse chronological order, except it will include a Announcements post
at the top of the Feed at all times.
* Must integrate with PostManagement System Component and Viewer Components to allow
user sto retrieve detailed and current information about a Feed entry by clicking on specific entries,
and allow them to update Feed by creating and editing posts.
clicking on the entry to populate the Viewer
* This population must happen practically instantaneously.
